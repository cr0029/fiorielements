jQuery.sap.declare("star4it.fiorielements.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("star4it.fiorielements.Component", {
	metadata: {
		"manifest": "json"
	}
});